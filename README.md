# Gab Post

Add a link to a gab in your blog post and this plugin will embed the gab.

### Prerequisites

WordPress

### Installing

This is a standard WordPress plugin. Copy the zipfile to your installation's
plugin folder or `git clone` it in there.

## Versioning

    yyyymmdd

## Authors

* **Matt Parrott** - *Initial work* - [@parrott](https://gab.ai/parrott)

## License

This project is licensed under the MIT License, because the GPL is a Marxist
conspiracy.

## Donation

Bitcoin: [13mjZhUfrPQojTZVTVmpqNQGTfrk1NRbsa](bitcoin://13mjZhUfrPQojTZVTVmpqNQGTfrk1NRbsa)

## Acknowledgments

* [Gab](https://gab.ai)
