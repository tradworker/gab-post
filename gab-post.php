<?php
/*
Plugin Name: Gab Post
Description: Display Gab.ai posts in your blog
Version: 20171122
*/

class Gab_Post {

	private $version = '20171122';

	private $debug = false;

	public function __construct( $debug = false ) {
		$this->debug = $debug;

		// bust cache
		if( $this->debug ) {
			$this->version = rand( 14, 1488 );
		}

		add_action( 'wp_enqueue_scripts', array( &$this, 'do_style' ) );

		add_filter( 'the_content', array( &$this, 'do_filter' ) );
	}

	public function do_style() {

		wp_enqueue_style(
			'gab-post',
			plugin_dir_url( __FILE__ ) . 'style/gab-post.css',
			array(),
			$this->version
		);

	}

	public function do_filter( $content = '' ) {

		if( preg_match( '/https?:\/\/gab\.ai\/([^\/]+)\/posts\/([0-9]+)/', $content, $matches ) ) {

			if( false === ( $gab_html  = $this->get_gab_html( $matches[0], $matches[2] ) ) ) {
				return $content;
			}

			if( false === ( $gab_array = $this->get_gab_array( $gab_html ) ) || empty( $gab_array ) ) {
				return $content;
			}

			$gab_post = $this->get_gab_post( $gab_array, $matches[1], $matches[2] );

			$content = str_replace( $matches[0], $gab_post, $content );

		}

		return $content;
	}

	private function get_gab_post( $gab_array, $gab_user, $gab_id ) {

		$gab_post_text = nl2br( $gab_array['og:description'] );

		$gab_post = <<<POST

<div class="gab-post gab-post-wrapper" id="gab-post-{$gab_id}">
	<span class="gab-post-title">
		<a href="{$gab_array['og:url']}">
			{$gab_array['og:title']}
		</a>
		&middot;
		<a href="https://gab.ai/{$gab_user}">
			@{$gab_user}
		</a>
	</span>
	<div class="gab-post-main">

		<a href="{$gab_array['og:url']}">
			<img src="{$gab_array['og:image']}" />
		</a>

		<span class="gab-post-text">
			{$gab_post_text}
		</span>

	</div>
</div>

POST;

		return $gab_post;
	}

	private function get_gab_array( $gab_post_html ) {

		$gab_post_dom = new DOMDocument();
		@$gab_post_dom->loadHTML( $gab_post_html );

		$meta_elements = $gab_post_dom->getElementsByTagName('meta');

		$gab_array = array();
		foreach( $meta_elements as $meta_element ) {

			if( $property = $meta_element->getAttribute( 'property' ) ) {
				if( strcmp( $property, 'og:' ) ) {

					$gab_array[$property] = $meta_element->getAttribute( 'content' );

				}
			}

		}

		return $gab_array;
	}

	private function get_gab_html( $link, $gab_post ) {

		if ( false === ( $gab_post_html = get_transient( "gab-post-{$gab_post}" ) ) ) {

			$gab_post_html = file_get_contents( $link );

			set_transient( "gab-post-{$gab_post}", $gab_post_html, 60 * 60 * 3 ); // three hours
		}

		return $gab_post_html;
	}

}

$gab_post = new Gab_Post( false );

/* EOF */
